package com.diptika.dam.smsapplication;

/**
 * Created by Diptika on 5/5/2016.
 */
import android.app.Application;
import com.parse.Parse;

public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Parse.initialize(this, "app-id", "client-key");
    }
}
